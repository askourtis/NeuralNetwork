import numpy as np
# TODO: Implement type hinting
# TODO: Runtime value and type checking
# TODO: Add math functions to different module

def sigmoid(x):
    """Math function"""
    return 1/(1+np.exp(-x))
    
def sigmoid_prime(x):
    """Math function"""
    return x*(1-x)
    
    
class Network:
    """A representation of a neural network"""

    def __init__(self, *layer_sizes, random_seed=None):
        """
        A constructor.

        Constructs the Network object with the given parameters.
        Initializes the weights matrix via the numpy.random.uniform function.

        Parameters
        ----------
        self : Network
            Implicitly given when calling the attribute.
        layer_sizes : Non-Keyworded-Variable-Length-List[int]
            The layer sizes of the network.

        Returns
        -------
        Network
            The constructed object.
        """
        if random_seed:
            np.random.seed(random_seed)

        self.weights = []
        self.biases = []
        for prev, curr in zip(layer_sizes[:-1],layer_sizes[1:]):
            self.weights.append(np.random.uniform(size=(prev, curr)))
            self.biases.append(np.random.uniform(size=curr))
    
    
    def train(self, input, output, iterations, learning_rate=0.1):
        """
        Trains the network via backpropagation.

        Calculates/Predicts an output and the error of that prediction and by backpropagating the error reajusts the weight and bias matrices.
        The function also returns an array containing the error sum of every iteration.

        Parameters
        ----------
        self : Network
            Implicitly given when calling the attribute.
        input : numpy.ndarray
            An array with dimensions NxM, where N is an arbitrary interger and M is the size of the input layer.
        output : numpy.ndarray
            An array with dimensions NxK, where N is the arbitrary input size and K is the size of the output layer.
        iterations : int
            An integer representing the number of iterations over the training sample.
        learning_rate : float
            Typicaly a small value, it adjusts the learning rate of the neural network. A high learning rate leans to a steep learning curve but also to an 'immature' network.

        Returns
        -------
        List[float]
            A list containing the error sum of every iteration.
        """
        ret = []
        for _ in range(iterations):
            layers = self._full_predict(input)  # forward pass
            prediction = layers.pop()

            # backpropagation
            error = (output - prediction) * sigmoid_prime(prediction) * learning_rate
            self.weights[-1] += np.dot(layers[-1].T, error) # TODO: divide by len(error) = N, the arbitrary input size?
            self.biases[-1] += sum(error)/len(error)
            
            
            for i in range(-2, -(len(self.weights)+1), -1):
                error = np.dot(error, self.weights[i + 1].T) * sigmoid_prime(layers[i + 1])
                self.weights[i] += np.dot(layers[i].T, error) # TODO: divide by len(error)?
                self.biases[i] += sum(error)/len(error)
            # partialy trained
            
            ret.append(sum((prediction - output)**2.0)/2)   # Cost function: Sum of squared differences
        return ret
    
    
    def _full_predict(self, input):
        """
        Calculates all the layers based on a given input.

        Parameters
        ----------
        self : Network
            Implicitly given when calling the attribute.
        input : numpy.ndarray
            An array with dimensions NxM, where N is an arbitrary interger and M is the size of the input layer.

        Returns
        -------
        List[numpy.ndarray]
            A list containing the outputs for all the layers of the network, including the input layer and the last output layer.
        """
        ret = [input]
        for W, B in zip(self.weights, self.biases):
            ret.append(sigmoid(np.dot(ret[-1], W) + B))
        return ret
        
        
    def predict(self, input):
        """
        Predicts an output based on an input.

        Calculates an output by multiplying the weight matrices on the input.
        Effectively 'predicting' an output.

        Parameters
        ----------
        self : Network
            Implicitly given when calling the attribute.
        input : numpy.ndarray
            An array with dimensions NxM, where N is an arbitrary interger and M is the size of the input layer.

        Returns
        -------
        numpy.ndarray
            An array with dimensions NxK, where N is the arbitrary input size and K is the size of the output layer.
        """
        return self._full_predict(input)[-1]

#EOF