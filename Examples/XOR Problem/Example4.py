import sys
sys.path.insert(1, '../../Source')

import matplotlib.pyplot as plt
import numpy as np
from NeuralNetwork import *



X = np.array([[0,0], [0,1], [1,0], [1,1]])
Y = np.array([ [0],   [1],   [1],   [0] ])
#The XOR problem


network = Network(2,4,1,random_seed=1)
network.train(X,Y,50000)

print("Trained")

img = []
for i in range(100):
    row = []
    for j in range(100):
        row.append(int(network.predict( np.array([[i,j]]) * 0.01 ) * 255))
    img.append(row)

plt.imshow(img)
plt.show()  
