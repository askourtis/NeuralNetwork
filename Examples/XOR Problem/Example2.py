import sys
sys.path.insert(1, '../../Source')

import matplotlib.pyplot as plt
import numpy as np
from NeuralNetwork import *



X = np.array([[0,0], [0,1], [1,0], [1,1]])
Y = np.array([ [0],   [1],   [1],   [0] ])
#The XOR problem


network = Network(2, 4, 1, random_seed=1)    #2 inputs, 4 hidden nodes, 1 output
data = [v[0] for v in network.train(X, Y, 1000)]   #train the network with input=X, output=Y, iterations=1000, learning_rate=default(0,1)
plt.figure(1)
plt.plot(list(range(len(data))), data)

network = Network(2, 4, 1, random_seed=1)    #2 inputs, 4 hidden nodes, 1 output
data = [v[0] for v in network.train(X, Y, 50000)]    #train the network with input=X, output=Y, iterations=50000, learning_rate=default(0,1)
plt.figure(2)   # same as figure(1) but zoomed out
plt.plot(list(range(len(data))), data)
plt.show()
