import sys
sys.path.insert(1, '../../Source')

import numpy as np
from NeuralNetwork import *

X = np.array([[0,0], [0,1], [1,0], [1,1]]) # 4x2
Y = np.array([ [0],   [1],   [1],   [0] ]) # 4x1
#The XOR problem

network = Network(2, 4, 1, random_seed=1)    #2 inputs, 4 hidden nodes, 1 output, random_seed = 1
network.train(X, Y, 50000)   #train the network with input=X, output=Y, iterations=50000, learning_rate=default(0,1)
print("Trained")

print("Accepting input...")
while True:
    inputs = [[float(input("First number: ")), float(input("Second Number: "))]]
    print(f"Prediction({inputs})= {network.predict(np.array(inputs))}")
