import sys
sys.path.insert(1, '../../Source')

import matplotlib.pyplot as plt
import numpy as np
from NeuralNetwork import *



X = np.array([[0,0], [0,1], [1,0], [1,1]])
Y = np.array([ [0],   [1],   [1],   [0] ])
#The XOR problem



for LR in np.arange(0.1, 1.1, 0.1):
    data = [v[0] for v in Network(2,4,1,random_seed=1).train(X, Y, 3000, LR)]
    plt.plot(list(range(len(data))), data, label=str(LR))
    
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)    
plt.show()
